﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdinaryNotes.Models
{
    public class CreateNoteModel
    {
        public string name { get; set; }
        public string text { get; set; }
        public string date { get; set; }
    }
}