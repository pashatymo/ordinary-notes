﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdinaryNotes.Models;

namespace OrdinaryNotes.Controllers
{
    public class HomeController : Controller
    {
        [HttpPost]
        public void CreateNote(CreateNoteModel newNote)
        {
            using (OrdinaryNotesEntities db = new OrdinaryNotesEntities())
            {
                db.Table_1.Add(new Table_1 { name = newNote.name, text = newNote.text, date = DateTime.Now}); 
                db.SaveChanges(); 
            }
        }

        [HttpGet]
        public void DeleteNoteById(string id)

        {
            var currentId = Convert.ToInt32(id);
            using (OrdinaryNotesEntities db = new OrdinaryNotesEntities())
            {
                var itemForDel = db.Table_1.SingleOrDefault(x => x.id == currentId);
                if (itemForDel != null)
                {
                    db.Table_1.Remove(itemForDel);
                    db.SaveChanges();
                }
            }
        }

         public ActionResult Index()
        {
            using (OrdinaryNotesEntities db = new OrdinaryNotesEntities())
            {
                return View(db.Table_1.ToList());
            }  
        }
    }
}